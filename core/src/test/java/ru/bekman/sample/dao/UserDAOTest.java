package ru.bekman.sample.dao;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.bekman.sample.cfg.DaoFactory;
import ru.bekman.sample.model.User;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class UserDAOTest {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final UserDAO dao = DaoFactory.getUserDAO();
    private final String userId = "123";

    @Before
    public void setUp() {
        dao.delete(userId);
    }

    @Test
    public void crudScenario() {
        assertThat(dao.getById(userId), equalTo(Optional.empty()));

        User user = User.builder().id(userId).firstName("Ivan").lastName("Ivanov").build();
        dao.save(user);
        assertThat(dao.getById(userId), equalTo(Optional.of(user)));

        user = user.toBuilder().lastName("Petrov").build();
        dao.update(user);
        assertThat(dao.getById(userId), equalTo(Optional.of(user)));

        dao.delete(userId);
        assertThat(dao.getById(userId), equalTo(Optional.empty()));
    }
}
