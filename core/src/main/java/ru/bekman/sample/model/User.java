package ru.bekman.sample.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class User {
    String id;
    String firstName;
    String lastName;
}
