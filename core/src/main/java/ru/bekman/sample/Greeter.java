package ru.bekman.sample;

import ru.bekman.sample.processor.GreetMessagePostProcessorFactory;

public class Greeter {

    public String createGreetMessage(String name) {
        return GreetMessagePostProcessorFactory.createPostProcessor().process("Hello, " + name);
    }
}
