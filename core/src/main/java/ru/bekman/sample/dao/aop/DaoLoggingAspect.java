package ru.bekman.sample.dao.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Aspect
public class DaoLoggingAspect {

    @Around("execution(* ru.bekman.sample.dao.impl.*.*(..))")
    public Object logDbAccessOperationDuration(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Signature signature = proceedingJoinPoint.getSignature();
        String targetDaoMethod =
                String.format("%s.%s", signature.getDeclaringType().getSimpleName(), signature.getName());

        long executionStart = System.currentTimeMillis();
        try {
            return proceedingJoinPoint.proceed();
        } finally {
            long durationMs = System.currentTimeMillis() - executionStart;
            log.info("DAO method {} execution took {} ms", targetDaoMethod, durationMs);
        }
    }
}
