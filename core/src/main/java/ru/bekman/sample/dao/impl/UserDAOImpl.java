package ru.bekman.sample.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.bekman.sample.dao.UserDAO;
import ru.bekman.sample.dao.impl.model.UserData;
import ru.bekman.sample.model.User;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public class UserDAOImpl implements UserDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    @Override
    public Optional<User> getById(String userId) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(UserData.class, userId)).map(this::map);

    }

    private User map(UserData user) {
        return User.builder().id(user.getId()).firstName(user.getFirstName()).lastName(user.getLastName()).build();
    }

    private UserData map(User user) {
        return UserData.builder().id(user.getId()).firstName(user.getFirstName()).lastName(user.getLastName()).build();
    }

    @Transactional
    @Override
    public void save(User user) {
        sessionFactory.getCurrentSession().persist(map(user));
    }

    @Transactional
    @Override
    public void update(User user) {
        sessionFactory.getCurrentSession().merge(map(user));
    }

    @Transactional
    @Override
    public void delete(String userId) {
        Query query = sessionFactory.getCurrentSession().createQuery("delete from UserData where id = :id");
        query.setParameter("id", userId);
        query.executeUpdate();

    }
}
