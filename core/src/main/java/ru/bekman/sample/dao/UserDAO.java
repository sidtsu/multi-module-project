package ru.bekman.sample.dao;

import ru.bekman.sample.model.User;

import java.util.Optional;

public interface UserDAO {
    Optional<User> getById(String userId);
    void save(User user);
    void update(User user);
    void delete(String userId);
}
