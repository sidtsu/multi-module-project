package ru.bekman.sample.processor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GreetMessagePostProcessorFactory {

    private GreetMessagePostProcessorFactory() {
    }

    public static GreetMessagePostProcessor createPostProcessor() {
        ClassLoader classLoader = GreetMessagePostProcessorFactory.class.getClassLoader();
        try (InputStream stream = classLoader
                .getResourceAsStream("META-INF/greet-post-processor.properties")) {

            Properties properties = new Properties();
            properties.load(stream);

            String processorClassName = properties.getProperty("implementation");
            return (GreetMessagePostProcessor) classLoader.loadClass(processorClassName).newInstance();

        } catch (Exception exc) {
            throw new RuntimeException("Failed to instantiate Greet Message Post Processor", exc);
        }

    }
}
