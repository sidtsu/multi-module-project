package ru.bekman.sample.processor;

public interface GreetMessagePostProcessor {

    String process(String message);
}
