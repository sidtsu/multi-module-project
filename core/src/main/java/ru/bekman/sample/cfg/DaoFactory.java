package ru.bekman.sample.cfg;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.bekman.sample.dao.UserDAO;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DaoFactory {

    public static UserDAO getUserDAO() {
        return SpringContextHolder.getBeanByType(UserDAO.class);
    }
}
