package ru.bekman.sample.cfg;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringContextHolder {

    private static final ConfigurableApplicationContext context;
    static {
        context = new AnnotationConfigApplicationContext(AppBeans.class);
    }

    public static <T> T getBeanByType(Class<T> klass) {
        return context.getBean(klass);
    }
}
