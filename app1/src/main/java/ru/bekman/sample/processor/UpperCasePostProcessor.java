package ru.bekman.sample.processor;

import java.util.Objects;

public class UpperCasePostProcessor implements GreetMessagePostProcessor {

    @Override
    public String process(String message) {
        Objects.requireNonNull(message, "Can't process null message");
        return message.toUpperCase();
    }
}
